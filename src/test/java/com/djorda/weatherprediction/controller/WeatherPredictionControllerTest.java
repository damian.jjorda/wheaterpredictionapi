package com.djorda.weatherprediction.controller;

import static org.mockito.Mockito.when;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.documentationConfiguration;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.delete;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.get;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.post;
import static org.springframework.restdocs.operation.preprocess.Preprocessors.preprocessRequest;
import static org.springframework.restdocs.operation.preprocess.Preprocessors.preprocessResponse;
import static org.springframework.restdocs.operation.preprocess.Preprocessors.prettyPrint;
import static org.springframework.restdocs.request.RequestDocumentation.parameterWithName;
import static org.springframework.restdocs.request.RequestDocumentation.pathParameters;
import static org.springframework.restdocs.request.RequestDocumentation.requestParameters;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Arrays;

import com.djorda.weatherprediction.WeatherpredictionApplication;
import com.djorda.weatherprediction.dao.WeatherPredictionDayDAO;
import com.djorda.weatherprediction.dto.WeatherPredictionDayDTO;
import com.djorda.weatherprediction.model.WeatherPredictionDay;
import com.djorda.weatherprediction.model.WeatherType;
import com.google.cloud.Timestamp;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.restdocs.JUnitRestDocumentation;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = WeatherpredictionApplication.class)
public class WeatherPredictionControllerTest {

	@Rule
	public final JUnitRestDocumentation restDocumentation = new JUnitRestDocumentation("target/generated-snippets");

    @Mock
    private WeatherPredictionDayDAO dao;

	@InjectMocks
	private WeatherPredictionController controller;

	private MockMvc mockMvc;

	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
		this.mockMvc = MockMvcBuilders
				.standaloneSetup(controller)
				.apply(documentationConfiguration(this.restDocumentation))
				.alwaysDo(document("{method-name}", preprocessRequest(prettyPrint()), preprocessResponse(prettyPrint())))
				.build();
	}

	/**
	 * Must return a predicted day
	 * @throws Exception
	 */
	@Test
    public void testGet() throws Exception {
		WeatherPredictionDayDTO dto = new WeatherPredictionDayDTO(1, WeatherType.DROUGHT);
        when(dao.getByDay(1)).thenReturn(dto);

        mockMvc.perform(get("/clima").param("dia", "1"))
				.andExpect(status().isOk())
				.andDo(document("test-get",
						requestParameters(
							parameterWithName("dia").description("The number of the day to get the predicted weather, starting from 0.")
				)));
	}
	
	/**
	 * Must return a list of weathers
	 * @throws Exception
	 */
	@Test
    public void testGetWeathers() throws Exception {
		WeatherPredictionDayDTO dto = new WeatherPredictionDayDTO(1, WeatherType.DROUGHT);
		WeatherPredictionDayDTO dto2 = new WeatherPredictionDayDTO(2, WeatherType.DROUGHT);
        when(dao.getByWeather("sequia")).thenReturn(Arrays.asList(dto, dto2));

        mockMvc.perform(get("/clima/{weatherType}", "sequia"))
				.andExpect(status().isOk())
				.andDo(document("test-get-weathers",
						pathParameters(
							parameterWithName("weatherType").description("The type of the weather to return all the days with that condition. It can be one of the following values: \"No hay informacion\",\"sequia\",\"lluvia\",\"pico maximo de lluvia\",\"condicion optima de presion y temperatura\",\"soleado\"")
				)));
	}
	
	/**
	 * Must return all the predicted weathers as they were saved in the database
	 * @throws Exception
	 */
	@Test
    public void testGetAll() throws Exception {
		WeatherPredictionDay entity = new WeatherPredictionDay(new Long(1), Timestamp.now(), 1, WeatherType.DROUGHT);
		WeatherPredictionDay entity2 = new WeatherPredictionDay(new Long(2), Timestamp.now(), 2, WeatherType.RAIN);
        when(dao.getAll()).thenReturn(Arrays.asList(entity, entity2));

        mockMvc.perform(get("/clima/todos"))
				.andExpect(status().isOk());
	}
	
	/**
	 * Must run an async runnnable that will predict all the weathers for the next ten years
	 * @throws Exception
	 */
	@Test
    public void testPredictAll() throws Exception {
        this.mockMvc.perform(post("/clima/predecir"))
            .andExpect(status().isOk());
	}
	
	/**
	 * Must run an async runnnable that will predict all the weathers for the next ten years
	 * @throws Exception
	 */
	@Test
    public void testPredictOne() throws Exception {
		WeatherPredictionDay entity = new WeatherPredictionDay(new Long(1), Timestamp.now(), 1, WeatherType.DROUGHT);
		when(dao.save(new Long(1))).thenReturn(entity);

        this.mockMvc.perform(post("/clima/predecir/{numberDay}", 1))
			.andExpect(status().isOk())
			.andDo(document("test-predict-one",
			pathParameters(
				parameterWithName("numberDay").description("The number of the day to predict the weather, starting from 0.")
			)));
	}
	
	/**
	 * Must delete one the entity
	 * @throws Exception
	 */
	@Test
    public void testDeleteOne() throws Exception {
        this.mockMvc.perform(delete("/clima/borrar/{id}", 1))
			.andExpect(status().isOk())
			.andDo(document("test-delete-one",
			pathParameters(
				parameterWithName("id").description("ID of the predicted weather to be deleted")
			)));
	}
	
	/**
	 * Must  delete all the entities
	 * @throws Exception
	 */
	@Test
    public void testDeleteAll() throws Exception {
        this.mockMvc.perform(delete("/clima/borrar", 1))
			.andExpect(status().isOk());
    }
}
