package com.djorda.weatherprediction.dao;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.djorda.weatherprediction.dto.WeatherPredictionDayDTO;
import com.djorda.weatherprediction.model.CoruscantSolarSystemConfiguration;
import com.djorda.weatherprediction.model.WeatherPredictionDay;
import com.djorda.weatherprediction.model.WeatherType;
import com.djorda.weatherprediction.repository.WeatherPredictionDayRepository;
import com.google.cloud.Timestamp;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;

/**
 * Unit tests for <code>WeatherPredictionDayDAO</code> class
 * 
 * @see WeatherPredictionDayDAO
 * @author Damian Jorda
 * @since 1.0
 */
@RunWith(MockitoJUnitRunner.class)
public class WeatherPredictionDayDAOTest {

    @Spy
    @InjectMocks
    private WeatherPredictionDayDAO dao;

    @Mock
    private WeatherPredictionDayRepository repository;

    @Spy
    private CoruscantSolarSystemConfiguration config;

    @Before
    public void initMocks() throws Exception {
        MockitoAnnotations.initMocks(this);
    }

    private WeatherPredictionDay createDay(int dayNumber) {
        return new WeatherPredictionDay(new Long(1), Timestamp.now(), dayNumber, WeatherType.DROUGHT);
    }

    private WeatherPredictionDay createDay() {
        return this.createDay(1);
    }

    /**
     * Must save the predicted day if it is not predicted yet
     */
    @Test
    public void testSave() {
        WeatherPredictionDay expectedDay = this.createDay();
        when(repository.save(any())).thenReturn(expectedDay);
        WeatherPredictionDay actualDay = dao.save(new Long(1));
        assertEquals(expectedDay, actualDay);

        WeatherPredictionDay mockDay = this.createDay();
        doReturn(mockDay).when(repository).findByDayNumberOrderByDateAndDayNumberAsc(1);
        WeatherPredictionDay actualDay1 = dao.save(new Long(1));
        assertNull(actualDay1);
    }

    /**
     * Must save all
     */
    @Test
    public void testSaveAll() {
        WeatherPredictionDay day1 = this.createDay();
        WeatherPredictionDay day2 = this.createDay();
        List<WeatherPredictionDay> entities = Arrays.asList(day1, day2);
        dao.saveAll(entities);
        verify(repository, times(1)).saveAll(entities);
    }

    /**
     * Must delete
     */
    @Test
    public void testDelete() {        
        doReturn(new ArrayList<WeatherPredictionDay>()).when(repository).findFirstByOrderByDayNumberDescDateDesc();
        dao.deleteAll();
        verify(repository, times(0)).deleteAll(any());

        doReturn(Arrays.asList(this.createDay(1500))).when(repository).findFirstByOrderByDayNumberDescDateDesc();
        dao.deleteAll();
        verify(repository, times(3)).deleteAll(any());

        dao.deleteById(new Long(1));
        verify(repository, times(1)).deleteById(new Long(1));
    }

    /**
     * Must return a list of weather prediction day DTO
     * 
     * @see WeatherPredictionDayDTO
     */
    @Test
    public void testGetByWeather() {
        WeatherPredictionDay day1 = this.createDay(1);
        WeatherPredictionDay day2 = this.createDay(2);
        WeatherPredictionDayDTO daydto1 = new WeatherPredictionDayDTO(1, WeatherType.DROUGHT);
        WeatherPredictionDayDTO daydto2 = new WeatherPredictionDayDTO(2, WeatherType.DROUGHT);
        List<WeatherPredictionDay> expectedEntities = Arrays.asList(day1, day2);
        List<WeatherPredictionDayDTO> expectedDtos = Arrays.asList(daydto1, daydto2);
        doReturn(expectedEntities).when(repository).findByWeatherTypeOrderByDateAscDayNumberAsc(anyString());
        List<WeatherPredictionDayDTO> predictedDays = dao.getByWeather(WeatherType.DROUGHT.getWeatherCondition());
        assertEquals(expectedDtos, predictedDays);
    }

    /**
     * Must return a list of weather prediction day DTO
     * 
     * @see WeatherPredictionDayDTO
     */
    @Test
    public void testGetAll() {
        WeatherPredictionDay day1 = this.createDay(1);
        WeatherPredictionDay day2 = this.createDay(2);
        List<WeatherPredictionDay> expectedEntities = Arrays.asList(day1, day2);
        doReturn(expectedEntities).when(repository).findAllOrderByDateAndDayNumberAsc();
        List<WeatherPredictionDay> predictedDays = dao.getAll();
        assertEquals(expectedEntities, predictedDays);
    }

    /**
     * Must predict all the weather days for the next ten years
     * 
     * @throws Exception
     * 
     * @see WeatherPredictionDayDTO
     */
    @Test
    public void testPredictAll() throws Exception {
        // Testing prediction of ten days
        doReturn(new ArrayList<WeatherPredictionDay>()).when(repository).findFirstByOrderByDayNumberDescDateDesc();
        LocalDateTime date = LocalDateTime.of(2019, 1, 10, 0, 0);
        doReturn(date).when(dao).getCurrentDateInTenYears();
        dao.predictForTheNextTenYears();
        verify(repository, times(11)).save(any());
    }

    /**
     * Must predict all the weather days for the next ten years
     * 
     * @throws Exception
     * 
     * @see WeatherPredictionDayDTO
     */
    @Test
    public void testPredictAllSmallerDate() throws Exception {
        LocalDateTime date = LocalDateTime.of(2019, 1, 10, 0, 0);
        doReturn(date).when(dao).getCurrentDateInTenYears();
        // Test date of last entity bigger than predict day
        WeatherPredictionDay day1 = this.createDay(1500);
        WeatherPredictionDay day2 = this.createDay(1501);
        doReturn(Arrays.asList(day1, day2)).when(repository).findFirstByOrderByDayNumberDescDateDesc();  
        dao.predictForTheNextTenYears();
        verify(repository, times(0)).save(any());
    }

    /**
     * Must predict all the weather days for the next ten years
     * 
     * @throws Exception
     * 
     * @see WeatherPredictionDayDTO
     */
    @Test
    public void testPredictAllBiggerDate() throws Exception {
        LocalDateTime date = LocalDateTime.of(2019, 1, 10, 0, 0);
        doReturn(date).when(dao).getCurrentDateInTenYears();
        // Test date of last entity bigger than predict day
        WeatherPredictionDay day1 = this.createDay(5);
        WeatherPredictionDay day2 = this.createDay(6);
        doReturn(Arrays.asList(day1, day2)).when(repository).findFirstByOrderByDayNumberDescDateDesc();  
        dao.predictForTheNextTenYears();
        verify(repository, times(5)).save(any());
    }
}