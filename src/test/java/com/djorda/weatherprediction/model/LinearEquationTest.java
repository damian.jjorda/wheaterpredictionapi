package com.djorda.weatherprediction.model;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import com.djorda.weatherprediction.utils.BigDecimalUtil;

import org.junit.Test;

/**
 * Unit tests for <code>LinearEquation</code> class
 * with the form <code>Y=mX+b</code>
 * @see LinearEquation
 * @author Damian Jorda
 * @since 1.0
 */
public class LinearEquationTest{

    /**
     * Must return the point A
     */
    @Test
    public void testPointA() {
        Point pointA = new Point(0, 0);
        Point pointB = new Point(1, 1);
        LinearEquation linearEquation = new LinearEquation(pointA, pointB);
        assertEquals(pointA, linearEquation.getPoint());      
    }

    /**
     * The b value and slope value must match for Points (0,0) and (1,1)
     * <p>
     * slope = (y2 - y1)/(x2 - x1), so is m = 1
     * <p>
     * b = Y - mx , so b = 0
     */
    @Test
    public void testSlopeAndB() {
        Point pointA = new Point(0, 0);
        Point pointB = new Point(1, 1);
        LinearEquation linearEquation = new LinearEquation(pointA, pointB);
        assertEquals(BigDecimalUtil.getBigDecimalWithScale(0), linearEquation.getB());
        assertEquals(BigDecimalUtil.getBigDecimalWithScale(1d), linearEquation.getSlope());       
    }

    /**
     * For vertical lines the slope doesn't exists
     * <p>
     * slope = (y2 - y1)/(x2 - x1), so is m = null
     * <p>
     * b = Y - mx , so b = null
     */
    @Test
    public void testVerticalLines() {
        // Over Y axis
        Point pointA = new Point(0, 1);
        Point pointB = new Point(0, 4);
        LinearEquation linearEquation = new LinearEquation(pointA, pointB);
        assertNull(linearEquation.getB());
        assertNull(linearEquation.getSlope());
        
        // Other Lines
        Point pointC = new Point(1, 1);
        Point pointD = new Point(1, 4);
        LinearEquation linearEquation1 = new LinearEquation(pointC, pointD);
        assertNull(linearEquation1.getB());
        assertNull(linearEquation1.getSlope());
    }

    /**
     * For horizontal lines 
     * <p>
     * slope = (y2 - y1)/(x2 - x1), so is m = 0
     * <p>
     * b = Y - mx , so over X axis b = 0
     * over other x != 0 b = Y
     */
    @Test
    public void testHorizontalLines() {
        // Over X axis
        Point pointA = new Point(1, 0);
        Point pointB = new Point(2, 0);
        LinearEquation linearEquation = new LinearEquation(pointA, pointB);
        assertEquals(BigDecimalUtil.getBigDecimalWithScale(0), linearEquation.getB());
        assertEquals(BigDecimalUtil.getBigDecimalWithScale(0), linearEquation.getSlope());
        
        // Other Lines
        Point pointC = new Point(1, 1);
        Point pointD = new Point(4, 1);
        LinearEquation linearEquation1 = new LinearEquation(pointC, pointD);
        assertEquals(BigDecimalUtil.getBigDecimalWithScale(1), linearEquation1.getB());
        assertEquals(BigDecimalUtil.getBigDecimalWithScale(0), linearEquation1.getSlope());
    }

    /**
     * test if a Point belongs to a Linear Equation
     */
    @Test
    public void testIfPointsBelongs() {
        // Over Y axis
        Point pointA = new Point(0, 1);
        Point pointB = new Point(0, 4);
        Point pointBelongsA = new Point(0, 5);
        Point pointBelongsB = new Point(0, 0);
        LinearEquation linearEquation = new LinearEquation(pointA, pointB);
        assertTrue(linearEquation.belongs(pointBelongsA));
        assertTrue(linearEquation.belongs(pointBelongsB));
        assertTrue(linearEquation.belongs(pointA));
        assertTrue(linearEquation.belongs(pointB));
        assertFalse(linearEquation.belongs(new Point(1,0)));
        assertFalse(linearEquation.belongs(new Point(-10, 4)));

        // Over X axis
        Point pointC = new Point(1, 0);
        Point pointD = new Point(2, 0);
        Point pointBelongsC = new Point(5, 0);
        Point pointBelongsD = new Point(-1, 0);
        LinearEquation linearEquation1 = new LinearEquation(pointC, pointD);
        assertTrue(linearEquation1.belongs(pointBelongsC));
        assertTrue(linearEquation1.belongs(pointBelongsD));
        assertTrue(linearEquation1.belongs(pointC));
        assertTrue(linearEquation1.belongs(pointD));
        assertFalse(linearEquation1.belongs(pointA));
        assertFalse(linearEquation1.belongs(pointB));
                
        // Diagonal Lines
        Point pointE = new Point(1, 1);
        Point pointF = new Point(2, 2);
        Point pointBelongsE = new Point(0, 0);
        Point pointBelongsF = new Point(-1, -1);
        Point pointBelongsG = new Point(2, 2);
        LinearEquation linearEquation2 = new LinearEquation(pointE, pointF);
        assertTrue(linearEquation2.belongs(pointBelongsE));
        assertTrue(linearEquation2.belongs(pointBelongsF));
        assertTrue(linearEquation2.belongs(pointBelongsG));
        assertTrue(linearEquation2.belongs(pointE));
        assertTrue(linearEquation2.belongs(pointF));
        assertFalse(linearEquation2.belongs(pointC));
        assertFalse(linearEquation2.belongs(pointD));
    }

    /**
     * get Parallel Linear Equations 
     */
    @Test
    public void testParallelLinearEquations() {
        // Over Y axis
        Point pointA = new Point(0, 1);
        Point pointB = new Point(0, 4);
        Point parallelPoint = new Point(1, 5);
        LinearEquation linearEquation = new LinearEquation(pointA, pointB);
        LinearEquation parallelEquation = linearEquation.getParallelLinearEquation(parallelPoint);
        assertEquals(linearEquation.getB(), parallelEquation.getB());
        assertEquals(linearEquation.getSlope(), parallelEquation.getSlope());
        
        // Point is in the same Y coordinate as pointA
        Point parallelPoint3 = new Point(1, 1);
        LinearEquation parallelEquation3 = linearEquation.getParallelLinearEquation(parallelPoint3);
        assertEquals(linearEquation.getB(), parallelEquation3.getB());
        assertEquals(linearEquation.getSlope(), parallelEquation3.getSlope());

        // Over X axis
        Point pointC = new Point(1, 0);
        Point pointD = new Point(2, 0);        
        Point parallelPoint1 = new Point(5, 5);
        LinearEquation linearEquation1 = new LinearEquation(pointC, pointD);
        LinearEquation parallelEquation1 = linearEquation1.getParallelLinearEquation(parallelPoint1);
        assertNotEquals(linearEquation1.getB(), parallelEquation1.getB());
        assertEquals(BigDecimalUtil.getBigDecimalWithScale(5), parallelEquation1.getB());
        assertEquals(linearEquation1.getSlope(), parallelEquation1.getSlope());

        // Over X axis and point over Y axis        
        Point pointG = new Point(0, 0);
        Point pointH = new Point(3, 0); 
        Point parallelPoint5 = new Point(0, 5);
        LinearEquation linearEquation5 = new LinearEquation(pointG, pointH);
        LinearEquation parallelEquation5 = linearEquation5.getParallelLinearEquation(parallelPoint5);
        assertNotEquals(linearEquation5.getB(), parallelEquation5.getB());
        assertEquals(BigDecimalUtil.getBigDecimalWithScale(5), parallelEquation5.getB());
        assertEquals(linearEquation5.getSlope(), parallelEquation5.getSlope());
                
        // Diagonal Lines
        Point pointE = new Point(1, 1);
        Point pointF = new Point(2, 2);
        Point parallelPoint2 = new Point(1, 0);
        LinearEquation linearEquation2 = new LinearEquation(pointE, pointF);
        LinearEquation parallelEquation2 = linearEquation2.getParallelLinearEquation(parallelPoint2);
        assertEquals(BigDecimalUtil.getBigDecimalWithScale(-1), parallelEquation2.getB());
        assertEquals(linearEquation2.getSlope(), parallelEquation2.getSlope());

        // same Point        
        LinearEquation parallelEquation4 = linearEquation2.getParallelLinearEquation(pointF);
        assertEquals(linearEquation2.getB(), parallelEquation4.getB());
        assertEquals(linearEquation2.getSlope(), parallelEquation4.getSlope());
    }


    /**
     *  Test parallel for diagonal LinearEquation
     */
    @Test
    public void testPointBetweenDiagonalLinearEquationAndParallelLinearEquation() {
        Point pointA = new Point(1, 1);
        Point pointB = new Point(2, 2);
        Point parallelPoint = new Point(0, 10);
        LinearEquation linearEquationA = new LinearEquation(pointA, pointB);
        LinearEquation linearEquationB = linearEquationA.getParallelLinearEquation(parallelPoint);       
        assertTrue(linearEquationB.belongs(new Point(1, 11)));
        assertTrue(linearEquationB.belongs(new Point(2, 12)));
        assertTrue(linearEquationB.belongs(new Point(3, 13)));
        assertTrue(linearEquationB.belongs(new Point(-1, 9)));
        assertTrue(linearEquationB.belongs(new Point(0, 10)));
        assertTrue(linearEquationB.belongs(new Point(-2, 8)));
        assertFalse(linearEquationB.belongs(pointA));
        assertFalse(linearEquationB.belongs(pointB));
    }

    /**
     * get Perpendicular Linear Equations 
     */
    @Test
    public void testPerpendicularFromVerticalLinearEquations() {
        // Over Y axis
        Point pointA = new Point(0, 1);
        Point pointB = new Point(0, 4);
        Point perpendicularlPoint = new Point(5, 0);
        LinearEquation linearEquation = new LinearEquation(pointA, pointB);
        LinearEquation perpendicularLinearEquation = linearEquation.getPerpendicularLinearEquation(perpendicularlPoint);
        assertEquals(BigDecimalUtil.getBigDecimalWithScale(0), perpendicularLinearEquation.getB());
        assertEquals(BigDecimalUtil.getBigDecimalWithScale(0), perpendicularLinearEquation.getSlope());
        
        LinearEquation perpendicularLinearEquationA = linearEquation.getPerpendicularLinearEquation(new Point(5,5));
        assertEquals(BigDecimalUtil.getBigDecimalWithScale(5), perpendicularLinearEquationA.getB());
        assertEquals(BigDecimalUtil.getBigDecimalWithScale(0), perpendicularLinearEquationA.getSlope());

        LinearEquation perpendicularLinearEquationB = linearEquation.getPerpendicularLinearEquation(new Point(-5,-5));
        assertEquals(BigDecimalUtil.getBigDecimalWithScale(-5), perpendicularLinearEquationB.getB());
        assertEquals(BigDecimalUtil.getBigDecimalWithScale(0), perpendicularLinearEquationB.getSlope());

        Point pointC = new Point(2, 1);
        Point pointD = new Point(2, 4);
        Point perpendicularlPointA = new Point(-5, 0);
        LinearEquation linearEquationA = new LinearEquation(pointC, pointD);
        LinearEquation perpendicularLinearEquationC = linearEquationA.getPerpendicularLinearEquation(perpendicularlPointA);
        assertEquals(BigDecimalUtil.getBigDecimalWithScale(0), perpendicularLinearEquationC.getB());
        assertEquals(BigDecimalUtil.getBigDecimalWithScale(0), perpendicularLinearEquationC.getSlope());
        
        LinearEquation perpendicularLinearEquationD = linearEquationA.getPerpendicularLinearEquation(new Point(5,5));
        assertEquals(BigDecimalUtil.getBigDecimalWithScale(5), perpendicularLinearEquationD.getB());
        assertEquals(BigDecimalUtil.getBigDecimalWithScale(0), perpendicularLinearEquationD.getSlope());

        LinearEquation perpendicularLinearEquationE = linearEquationA.getPerpendicularLinearEquation(new Point(-5,-5));
        assertEquals(BigDecimalUtil.getBigDecimalWithScale(-5), perpendicularLinearEquationE.getB());
        assertEquals(BigDecimalUtil.getBigDecimalWithScale(0), perpendicularLinearEquationE.getSlope());
    }

     /**
     * get Perpendicular Linear Equations 
     */
    @Test
    public void testPerpendicularFromHorizontalLinearEquations() {
        // Over X axis
        Point pointA = new Point(1, 0);
        Point pointB = new Point(2, 0);             
        Point perpendicularlPointA = new Point(0, 5);
        LinearEquation linearEquationA = new LinearEquation(pointA, pointB);
        LinearEquation perpendicularLinearEquationA = linearEquationA.getPerpendicularLinearEquation(perpendicularlPointA);
        assertNull(perpendicularLinearEquationA.getB());
        assertNull(perpendicularLinearEquationA.getSlope());
        
        LinearEquation perpendicularLinearEquationB = linearEquationA.getPerpendicularLinearEquation(new Point(5,5));
        assertNull(perpendicularLinearEquationB.getB());
        assertNull(perpendicularLinearEquationB.getSlope());

        LinearEquation perpendicularLinearEquationC = linearEquationA.getPerpendicularLinearEquation(new Point(-5,-5));
        assertNull(perpendicularLinearEquationC.getB());
        assertNull(perpendicularLinearEquationC.getSlope());

        Point pointC = new Point(1, 2);
        Point pointD = new Point(2, 2);        
        Point perpendicularlPointD = new Point(0, 5);
        LinearEquation linearEquationB = new LinearEquation(pointC, pointD);
        LinearEquation perpendicularLinearEquationD = linearEquationB.getPerpendicularLinearEquation(perpendicularlPointD);
        assertNull(perpendicularLinearEquationD.getB());
        assertNull(perpendicularLinearEquationD.getSlope());
        
        LinearEquation perpendicularLinearEquationE = linearEquationB.getPerpendicularLinearEquation(new Point(5,-5));
        assertNull(perpendicularLinearEquationE.getB());
        assertNull(perpendicularLinearEquationE.getSlope());

        LinearEquation perpendicularLinearEquationF = linearEquationB.getPerpendicularLinearEquation(new Point(-5,-5));        
        assertNull(perpendicularLinearEquationF.getB());
        assertNull(perpendicularLinearEquationF.getSlope());
    }

     /**
     * get Perpendicular Linear Equations 
     */
    @Test
    public void testPerpendicularFromDiagonalLinearEquations() {                
        // Diagonal Lines
        Point pointA = new Point(1, 1);
        Point pointB = new Point(2, 2);
        Point perpendicularlPointA = new Point(-1, 1);
        LinearEquation linearEquationA = new LinearEquation(pointA, pointB);
        LinearEquation perpendicularLinearEquationA = linearEquationA.getPerpendicularLinearEquation(perpendicularlPointA);
        assertEquals(BigDecimalUtil.getBigDecimalWithScale(0), perpendicularLinearEquationA.getB());
        assertEquals(BigDecimalUtil.getBigDecimalWithScale(-1), perpendicularLinearEquationA.getSlope());

        Point pointC = new Point(-1, 2);
        Point pointD = new Point(2, -1);
        Point perpendicularlPointB = new Point(-1, 0);
        LinearEquation linearEquationB = new LinearEquation(pointC, pointD);
        LinearEquation perpendicularLinearEquationB = linearEquationB.getPerpendicularLinearEquation(perpendicularlPointB);
        assertEquals(BigDecimalUtil.getBigDecimalWithScale(1), perpendicularLinearEquationB.getB());
        assertEquals(BigDecimalUtil.getBigDecimalWithScale(1), perpendicularLinearEquationB.getSlope());
    }

     /**
     * Perpendicular linear equation must not exist because the point belongs to the same linear equation
     */
    @Test
    public void testPerpendicularWithPointBelongs() {
        Point pointA = new Point(0, 1);
        Point pointB = new Point(0, 4);
        Point perpendicularlPoint = new Point(0, 0);
        LinearEquation linearEquation = new LinearEquation(pointA, pointB);
        LinearEquation perpendicularLinearEquation = linearEquation.getPerpendicularLinearEquation(perpendicularlPoint);
        assertNull(perpendicularLinearEquation);
    }

    /**
     * General test
     */
    @Test
    public void testGeneralBehavior() {
        Point pointA = new Point(1, 2);
        Point pointB = new Point(10, 2);
        Point perpendicularlPoint = new Point(3, 10);
        LinearEquation linearEquation = new LinearEquation(pointA, pointB);
        LinearEquation perpendicularLinearEquation = linearEquation.getPerpendicularLinearEquation(perpendicularlPoint);
        Point intersectionPoint = linearEquation.getIntersectionPoint(linearEquation, perpendicularlPoint);
        assertNull(perpendicularLinearEquation.getB());
        assertNull(perpendicularLinearEquation.getSlope());
        assertEquals(new Point(3,2), intersectionPoint);

        LinearEquation linearEquationA = new LinearEquation(pointA, perpendicularlPoint);
        LinearEquation perpendicularLinearEquationA = linearEquationA.getPerpendicularLinearEquation(pointB);
        Point intersectionPointA = linearEquationA.getIntersectionPoint(linearEquationA, pointB);
        assertEquals(BigDecimalUtil.getBigDecimalWithScale(4.5), perpendicularLinearEquationA.getB());
        assertEquals(BigDecimalUtil.getBigDecimalWithScale(-0.25d), perpendicularLinearEquationA.getSlope());

        LinearEquation linearEquationB = new LinearEquation(pointB, perpendicularlPoint);
        LinearEquation perpendicularLinearEquationB = linearEquationB.getPerpendicularLinearEquation(pointA);
        Point intersectionPointB = linearEquationB.getIntersectionPoint(linearEquationB, pointA);
        assertEquals(BigDecimalUtil.getBigDecimalWithScale(1.12), perpendicularLinearEquationB.getB());
        assertEquals(BigDecimalUtil.getBigDecimalWithScale(0.88), perpendicularLinearEquationB.getSlope());
    }

}