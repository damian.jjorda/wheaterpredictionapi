package com.djorda.weatherprediction.model;

import static org.junit.Assert.assertEquals;

import com.google.cloud.Timestamp;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;

/**
 * Unit tests for <code>WeatherPredictionDay</code> class
 * 
 * @see WeatherPredictionDay
 * @author Damian Jorda
 * @since 1.0
 */
@RunWith(MockitoJUnitRunner.class)
public class WeatherPredictionDayTest{


    /**
     * test constructors
     */
    @Test
    public void testConstructors() {
        WeatherPredictionDay model = new WeatherPredictionDay();
        assertEquals(model, new WeatherPredictionDay()); 
        Timestamp date = Timestamp.now();
        WeatherPredictionDay modelWithParameters = new WeatherPredictionDay(123l, date, 1, WeatherType.DROUGHT);        
        assertEquals(new Long(123), modelWithParameters.getId()); 
        assertEquals(date.toDate().toString(), modelWithParameters.getDate()); 
        assertEquals(1, modelWithParameters.getDayNumber()); 
        assertEquals(WeatherType.DROUGHT, modelWithParameters.getWeatherType()); 
    }

}