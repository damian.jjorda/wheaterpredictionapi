package com.djorda.weatherprediction.model;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;

/**
 * Unit tests for <code>SolarSystem</code> class
 * 
 * @see SolarSystem
 * @author Damian Jorda
 * @since 1.0
 */
@RunWith(MockitoJUnitRunner.class)
public class SolarSystemTest{

    @Spy
    private CoruscantSolarSystemConfiguration configuration;

    /**
     * Test quatity days for the given SolarSystem configuration
     */
    @Test
    public void testDaysForConfiguration() {
        assertEquals(360, configuration.getDaysInYear());   
    }

    /**
     * Test no data weather condition
     */
    @Test
    public void testNoDataWeather() {
        when(configuration.getPlanets()).thenReturn(new ArrayList<Planet>());
        SolarSystem solarSystem = new SolarSystem(configuration);
        assertEquals(WeatherType.NO_DATA, solarSystem.getWeather(0));   
    }

    /**
     * Test drought weather condition
     */
    @Test
    public void testDroughtWeather() {
        SolarSystem solarSystem = new SolarSystem(new CoruscantSolarSystemConfiguration());
        assertEquals(WeatherType.DROUGHT, solarSystem.getWeather(0));   
    }

    /**
     * Test rain, max rain and sunny weather condition
     */
    @Test
    public void testRainAndMaxRainAndSunnyWeather() {
        SolarSystem solarSystem = new SolarSystem(new CoruscantSolarSystemConfiguration());
        assertEquals(WeatherType.SUNNY, solarSystem.getWeather(1));   
        assertEquals(WeatherType.RAIN, solarSystem.getWeather(25));  
        assertEquals(WeatherType.MAX_RAIN, solarSystem.getWeather(72));  
    }

    /**
     * Test optimus weather condition
     */
    @Test
    public void testOptimusWeather() {   
        List<Planet> planets = new ArrayList<Planet>();
        planets.add(new Planet("alohomora", 0, 2 ));
        planets.add(new Planet("alohomora1", 90, 2 ));
        planets.add(new Planet("alohomora2", -Math.toDegrees(Math.acos(3 / Math.sqrt(10))), Math.sqrt(10) ));
        when(configuration.getPlanets()).thenReturn(planets);
        SolarSystem solarSystem1 = new SolarSystem(configuration);
        assertEquals(WeatherType.OPTIMUS, solarSystem1.getWeather(1));       
    }

}