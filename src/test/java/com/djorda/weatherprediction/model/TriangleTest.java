package com.djorda.weatherprediction.model;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

/**
 * Unit tests for <code>Triangle</code> class
 * @see Triangle
 * @author Damian Jorda
 * @since 1.0
 */
public class TriangleTest{

    /**
     * Must return the center of the triangle
     */
    @Test
    public void testCenter() {
        Point pointA = new Point(-3, 0);
        Point pointB = new Point(3, 0);
        Point pointC = new Point(0, 6);
        Triangle triangleA = new Triangle(pointA, pointB, pointC);
        assertEquals(new Point(0, 2), triangleA.getCenter()); 
        assertTrue(triangleA.isCenter(new Point(0, 2))); 
        assertFalse(triangleA.isCenter(new Point(1, 2)));
        
        Point pointD = new Point(0, -6);
        Triangle triangleB = new Triangle(pointA, pointB, pointD);
        assertEquals(new Point(0, -2), triangleB.getCenter());  
        assertTrue(triangleB.isCenter(new Point(0, -2))); 
        assertFalse(triangleB.isCenter(new Point(1, 2)));
        
        Point pointE = new Point(0, -3);
        Point pointF = new Point(0, 3);
        Point pointG = new Point(6, 0);
        Triangle triangleC = new Triangle(pointE, pointF, pointG);
        assertEquals(new Point(2, 0), triangleC.getCenter());  
        assertTrue(triangleC.isCenter(new Point(2, 0))); 
        assertFalse(triangleC.isCenter(new Point(2, 2))); 
        
        Point pointH = new Point(-6, 0);
        Triangle triangleD = new Triangle(pointE, pointF, pointH);
        assertEquals(new Point(-2, 0), triangleD.getCenter());  
        assertTrue(triangleD.isCenter(new Point(-2, 0))); 
        assertFalse(triangleD.isCenter(new Point(1, 10))); 
    }

    /**
     * Must return if the point belongs to the triangle
     */
    @Test
    public void testBelongs() {
        Point pointA = new Point(-3, 0);
        Point pointB = new Point(3, 0);
        Point pointC = new Point(0, 6);
        Triangle triangleA = new Triangle(pointA, pointB, pointC);
        assertTrue(triangleA.belongs(new Point(0, 2))); 
        
        Point pointD = new Point(0, -6);
        Triangle triangleB = new Triangle(pointA, pointB, pointD);
        assertTrue(triangleB.belongs(new Point(0, -2)));  
        
        Point pointE = new Point(0, -3);
        Point pointF = new Point(0, 3);
        Point pointG = new Point(6, 0);
        Triangle triangleC = new Triangle(pointE, pointF, pointG);
        assertTrue(triangleC.belongs(new Point(2, 0)));   
        
        Point pointH = new Point(-6, 0);
        Triangle triangleD = new Triangle(pointE, pointF, pointH);
        assertTrue(triangleD.belongs(new Point(-2, 0)));   
    }

    /**
     * Must return the perimeter
     */
    @Test
    public void testPerimeter() {
        Point pointA = new Point(-3, 0);
        Point pointB = new Point(3, 0);
        Point pointC = new Point(0, Math.sqrt(27));
        Triangle triangleA = new Triangle(pointA, pointB, pointC);
        assertEquals(18d, triangleA.getPerimeter(), 0.009d);  
    }

    /**
     * Max perimeter test
     */
    @Test
    public void testMaxPerimeter() {
        Point pointA = new Point(-3, 0);
        Point pointB = new Point(3, 0);
        Point pointC = new Point(0, Math.sqrt(27));
        Triangle triangleA = new Triangle(pointA, pointB, pointC);
        assertFalse(triangleA.isMaxPerimeter());  

        Point pointD = new Point(0, 6);
        Triangle triangleB = new Triangle(pointA, pointB, pointD);
        assertFalse(triangleB.isMaxPerimeter());  
    }

}