package com.djorda.weatherprediction.model;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import org.junit.Test;

/**
 * Unit tests for <code>WeatherType</code> class
 * 
 * @see WeatherType
 * @author Damian Jorda
 * @since 1.0
 */
public class WeatherTypeTest{

    private final int NO_DATA_INDEX = 0;
    private final int DROUGHT_INDEX = 1;
    private final int RAIN_INDEX = 2;
    private final int MAX_RAIN_INDEX = 3;
    private final int OPTIMUS_INDEX = 4;
    private final int SUNNY_INDEX = 5;

    private final String NO_DATA = "No hay informacion";
    private final String DROUGHT = "sequia";
    private final String RAIN = "lluvia";
    private final String MAX_RAIN = "pico maximo de lluvia";
    private final String OPTIMUS = "condicion optima de presion y temperatura";
    private final String SUNNY = "soleado";

    /**
     * The text for the weather type must match
     */
    @Test
    public void testWeatherTypesTexts() {
        assertEquals(NO_DATA, WeatherType.NO_DATA.getWeatherCondition());
        assertEquals(DROUGHT, WeatherType.DROUGHT.getWeatherCondition());
        assertEquals(RAIN, WeatherType.RAIN.getWeatherCondition());
        assertEquals(MAX_RAIN, WeatherType.MAX_RAIN.getWeatherCondition());
        assertEquals(OPTIMUS, WeatherType.OPTIMUS.getWeatherCondition());
        assertEquals(SUNNY, WeatherType.SUNNY.getWeatherCondition());
    }

    /**
     * The index for the weather type must match
     */
    @Test
    public void testWeatherTypesIndexes() {
        assertEquals(NO_DATA_INDEX, WeatherType.NO_DATA.ordinal());
        assertEquals(DROUGHT_INDEX, WeatherType.DROUGHT.ordinal());
        assertEquals(RAIN_INDEX, WeatherType.RAIN.ordinal());
        assertEquals(MAX_RAIN_INDEX, WeatherType.MAX_RAIN.ordinal());
        assertEquals(OPTIMUS_INDEX, WeatherType.OPTIMUS.ordinal());
        assertEquals(SUNNY_INDEX, WeatherType.SUNNY.ordinal());
    }

    /**
     * The string values for WeatherType enum
     */
    @Test
    public void testWeatherTypesStringValues() {
        assertEquals(WeatherType.NO_DATA, WeatherType.fromString(NO_DATA));
        assertEquals(WeatherType.DROUGHT, WeatherType.fromString(DROUGHT));
        assertEquals(WeatherType.RAIN, WeatherType.fromString(RAIN));
        assertEquals(WeatherType.MAX_RAIN, WeatherType.fromString(MAX_RAIN));
        assertEquals(WeatherType.OPTIMUS, WeatherType.fromString(OPTIMUS));
        assertEquals(WeatherType.SUNNY, WeatherType.fromString(SUNNY));
        assertNull(WeatherType.fromString(null));
        assertNull(WeatherType.fromString("fakeweather"));
    }

}