package com.djorda.weatherprediction.model;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.math.BigDecimal;

import com.djorda.weatherprediction.utils.BigDecimalUtil;

import org.junit.Test;

/**
 * Unit tests for <code>Point</code> class
 * 
 * @see Point
 * @author Damian Jorda
 * @since 1.0
 */
public class PointTest{

    /**
     * The given coordinates to create the #Point must not change
     */
    @Test
    public void testContainsCoordinates() {
        BigDecimal x = BigDecimalUtil.getBigDecimalWithScale(10);
        BigDecimal y = BigDecimalUtil.getBigDecimalWithScale(5);
        Point point = new Point(x, y);
        assertEquals(x, point.getX());
        assertEquals(y, point.getY());        
    }

    /**
     * Test different distances:
     * <p>
     * Point(0,0) to Point(0,1) is 1
     * <p>
     * Point(0,0) to Point(1,0) is 1
     * <p>
     * Point(0,0) to Point(1,1) is 1.41d
     */
    @Test
    public void testDistanceBetweenPoints() {
        Point centerPoint = new Point(0d, 0d);
        Point onXAxis = new Point(1d, 0d);
        Point onYAxis = new Point(0d, 1d);
        Point diagonalPoint = new Point(1d, 1d);

        assertEquals(BigDecimalUtil.getBigDecimalWithScale(1), centerPoint.distanceTo(onXAxis));
        assertEquals(BigDecimalUtil.getBigDecimalWithScale(1), centerPoint.distanceTo(onYAxis));  
        assertEquals(BigDecimalUtil.getBigDecimalWithScale(1.41), centerPoint.distanceTo(diagonalPoint));          
        
        Point pointA = new Point(3, 2);
        Point pointB = new Point(3, 10);
        assertEquals(pointA.distanceTo(pointB), pointB.distanceTo(pointA));

        Point pointC = new Point(1, 2);
        Point pointD = new Point(10, 2);
        assertEquals(pointC.distanceTo(pointD), pointD.distanceTo(pointC));
        assertEquals(pointA.distanceTo(pointC), pointC.distanceTo(pointA));
        assertEquals(pointA.distanceTo(pointD), pointD.distanceTo(pointA));
        assertEquals(pointC.distanceTo(pointB), pointB.distanceTo(pointC));
        assertEquals(pointD.distanceTo(pointB), pointB.distanceTo(pointD));
    }

    /**
     * Test equals Point.
     */
    @Test
    public void testEqualsPoints() {
        Point centerPoint = new Point(1d, 0d);
        Point samePoint = new Point(1d, 0d);
        Point xCoordinateChange = new Point(2d, 0d);
        Point yCoordinateChange = new Point(1d, 2d);

        assertTrue(centerPoint.equals(centerPoint));
        assertTrue(centerPoint.equals(samePoint));
        assertFalse(centerPoint.equals(null));   
        assertFalse(centerPoint.equals(new Double(1)));
        assertFalse(centerPoint.equals(xCoordinateChange));   
        assertFalse(centerPoint.equals(yCoordinateChange));         
    }
}