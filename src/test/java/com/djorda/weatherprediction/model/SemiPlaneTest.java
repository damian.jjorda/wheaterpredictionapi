package com.djorda.weatherprediction.model;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

/**
 * Unit tests for <code>SemiPlane</code> class
 * 
 * @see SemiPlane
 * @author Damian Jorda
 * @since 1.0
 */
public class SemiPlaneTest{

    /**
     * The point must belong to the SemiPlane
     */
    @Test
    public void testPointBetweenYAxisAndXSmallerOrEqualThan5() {
        Point pointA = new Point(0, 1);
        Point pointB = new Point(0, 4);
        Point parallelPoint = new Point(5, 5);
        LinearEquation linearEquationA = new LinearEquation(pointA, pointB);
        LinearEquation linearEquationB = linearEquationA.getParallelLinearEquation(parallelPoint);
        SemiPlane semiPlane = new SemiPlane(linearEquationA, linearEquationB);
        assertTrue(semiPlane.belongs(new Point(1, 1)));
        assertTrue(semiPlane.belongs(new Point(2, 2)));
        assertTrue(semiPlane.belongs(new Point(3, 20)));
        assertTrue(semiPlane.belongs(new Point(4, -10)));
        assertTrue(semiPlane.belongs(new Point(5, 9)));
        assertTrue(semiPlane.belongs(new Point(0, -1)));
        assertFalse(semiPlane.belongs(new Point(6, 5)));
        assertFalse(semiPlane.belongs(new Point(-1, -5)));
    }

    /**
     * The point must belong to the SemiPlane
     */
    @Test
    public void testPointBetweenXAxisAndYSmallerOrEqualThan10() {
        Point pointA = new Point(1, 0);
        Point pointB = new Point(2, 0);
        Point parallelPoint = new Point(5, 10);
        LinearEquation linearEquationA = new LinearEquation(pointA, pointB);
        LinearEquation linearEquationB = linearEquationA.getParallelLinearEquation(parallelPoint);
        SemiPlane semiPlane = new SemiPlane(linearEquationA, linearEquationB);         
        assertTrue(semiPlane.belongs(new Point(1, 1)));
        assertTrue(semiPlane.belongs(new Point(2, 2)));
        assertTrue(semiPlane.belongs(new Point(3, 10)));
        assertTrue(semiPlane.belongs(new Point(0, 10)));
        assertTrue(semiPlane.belongs(new Point(-3, 5)));
        assertTrue(semiPlane.belongs(new Point(-5, 0)));
        assertFalse(semiPlane.belongs(new Point(5, -5)));
        assertFalse(semiPlane.belongs(new Point(5, 11)));
    }

    /**
     * The point must belong to the SemiPlane
     */
    @Test
    public void testPointBetweenDiagonalLinearEquationAndParallelLinearEquation() {
        Point pointA = new Point(1, 1);
        Point pointB = new Point(2, 2);
        Point parallelPoint = new Point(0, 10);
        LinearEquation linearEquationA = new LinearEquation(pointA, pointB);
        LinearEquation linearEquationB = linearEquationA.getParallelLinearEquation(parallelPoint);
        SemiPlane semiPlane = new SemiPlane(linearEquationA, linearEquationB);         
        assertTrue(semiPlane.belongs(new Point(0, 7)));
        assertTrue(semiPlane.belongs(new Point(1, 8)));
        assertTrue(semiPlane.belongs(new Point(-3, -2)));
        assertTrue(semiPlane.belongs(new Point(5, 7)));
        assertTrue(semiPlane.belongs(new Point(-8, -7)));
        assertTrue(semiPlane.belongs(new Point(-8, -8)));
        assertTrue(semiPlane.belongs(new Point(-10, 0)));
        assertFalse(semiPlane.belongs(new Point(10, 0)));
        assertFalse(semiPlane.belongs(new Point(0, -15)));
    }

}