package com.djorda.weatherprediction.model;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

/**
 * Unit tests for <code>Planet</code> class
 * 
 * @see Planet
 * @author Damian Jorda
 * @since 1.0
 */
public class PlanetTest{

    private final int DAY_ZERO = 0;
    private final int DAY_NINETY = 90;
    private final int DAY_ONE_HUNDRED_AND_EIGHTY = 180;
    private final int DAY_TWO_HUNDRED_AND_SEVENTY = 270;
    private final int DAY_THREE_HUNDRED_AND_SIXTY = 360;

    /**
     * The name and the distance to the sun must be the same
     */
    @Test
    public void testNameAndDistanceToSun() {
        String name = "Vulcano";
        double distanceToSun = 1000;
        Planet planet = new Planet(name, 1, distanceToSun);
        assertEquals(name, planet.getName());
        assertEquals(distanceToSun, planet.getDistanceToSun(), 0.00d);        
    }

    /**
     * Test different positions of a planet. A planet with an angular velocity
     * of 1 degrees per day, at 0 day it must be in the x axis, at 90 degrees it must 
     * be in the y axis and go on..
     */
    @Test
    public void testPositionOfPlanet() {
        Planet planet = new Planet("Vulcano", 1, 1);
        Point dayZero = new Point(1, 0);
        Point dayNinety = new Point(0, 1);
        Point dayOneHundreAndEighty = new Point(-1, 0);
        Point dayTwoHundredAndSeventy = new Point(0, -1);
        Point dayThreeHundredAndSixty = new Point(1, 0);
        assertEquals(dayZero, planet.getPosition(DAY_ZERO));  
        assertEquals(dayNinety, planet.getPosition(DAY_NINETY));  
        assertEquals(dayOneHundreAndEighty, planet.getPosition(DAY_ONE_HUNDRED_AND_EIGHTY));  
        assertEquals(dayTwoHundredAndSeventy, planet.getPosition(DAY_TWO_HUNDRED_AND_SEVENTY));  
        assertEquals(dayThreeHundredAndSixty, planet.getPosition(DAY_THREE_HUNDRED_AND_SIXTY));      
    }
}