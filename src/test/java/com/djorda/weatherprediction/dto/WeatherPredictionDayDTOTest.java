package com.djorda.weatherprediction.dto;

import static org.junit.Assert.assertEquals;

import com.djorda.weatherprediction.model.WeatherType;

import org.junit.Test;

/**
 * Unit tests for <code>WeatherPredictionDayDTO</code> class
 * 
 * @see WeatherPredictionDayDTO
 * @author Damian Jorda
 * @since 1.0
 */
public class WeatherPredictionDayDTOTest{

    /**
     * Must return a weather type
     */
    @Test
    public void testGetWeather() {
        WeatherPredictionDayDTO dto = new WeatherPredictionDayDTO(1, WeatherType.DROUGHT);
        assertEquals(1, dto.getDay());
        assertEquals(WeatherType.DROUGHT.getWeatherCondition(), dto.getWeather());        
    }

}