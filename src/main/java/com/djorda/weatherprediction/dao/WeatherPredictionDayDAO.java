package com.djorda.weatherprediction.dao;

import java.sql.Date;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

import com.djorda.weatherprediction.dto.WeatherPredictionDayDTO;
import com.djorda.weatherprediction.model.CoruscantSolarSystemConfiguration;
import com.djorda.weatherprediction.model.SolarSystem;
import com.djorda.weatherprediction.model.WeatherPredictionDay;
import com.djorda.weatherprediction.model.WeatherType;
import com.djorda.weatherprediction.repository.WeatherPredictionDayRepository;
import com.google.cloud.Timestamp;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 * Data Acces Object for Weather Prediction Day controller 
 * @since 1.0
 * @author Damian Jorda
 */
@Repository
public class WeatherPredictionDayDAO {
    
    private final int TEN_YEARS = 10;

    private WeatherPredictionDayRepository weatherPredictionDayRepository;
    private SolarSystem solarSystem;

    /**
     * Create and initializes a WeatherPredictionDAO
     * 
     * @param weatherPredictionDayRepository to do database operations
     * @param config for the current Solar System
     */
    @Autowired
    public WeatherPredictionDayDAO(WeatherPredictionDayRepository weatherPredictionDayRepository, CoruscantSolarSystemConfiguration config){
        this.solarSystem = new SolarSystem(config);
        this.weatherPredictionDayRepository = weatherPredictionDayRepository;     
    }

    /**
     * Given a day, it will be predicted and will be saved in the database.
     * 
     * @param day to predict
     * @return the predicted day
     */
    public WeatherPredictionDay save(Long day) {
        WeatherPredictionDayDTO predictedDay = this.getByDay(day.intValue());
        if(predictedDay != null){
            return null;
        }
        LocalDateTime dateTime = this.getNewDateInstance();              
        dateTime = dateTime.plusDays(day);
        WeatherPredictionDay weatherPredictionDay = createWeatherPredictionDayEntity(day, dateTime);
        return this.weatherPredictionDayRepository.save(weatherPredictionDay);
    }

    /**
     * Receives a list of days and save them all in the database.
     * @param predictedDays a list of predicted days to be saved in the database
     */
    public void saveAll(List<WeatherPredictionDay> predictedDays) {
        this.weatherPredictionDayRepository.saveAll(predictedDays);
    }

    /**
     * Create a WeatherPredictionDay entity.
     * @param day for the predicted weather
     * @param dateTime for the predicted weather
     * @return the entity predicted
     * @see WeatherPredictionDay
     */
    private WeatherPredictionDay createWeatherPredictionDayEntity(Long day, LocalDateTime dateTime ) {
        WeatherPredictionDay weatherPredictionDay = new WeatherPredictionDay();
        weatherPredictionDay.setDayNumber(day.intValue());
        weatherPredictionDay.setWeatherType(solarSystem.getWeather(day.intValue()));
        weatherPredictionDay.setDate(Timestamp.of(Date.valueOf(dateTime.toLocalDate())));
        return weatherPredictionDay;
    }

    /**
     * Delete an entity in the database by the given id.
     * @param id of the WeatherPredictionDay to be deleted
     */
    public void deleteById(Long id){
        this.weatherPredictionDayRepository.deleteById(id);
    }

    /**
     * Delete all the weather prediction day entities in the database
     */
    public void deleteAll(){
        // Limited by the free api so I have to delete by 500 entities
        List<WeatherPredictionDay> lastDayPredicted =  this.weatherPredictionDayRepository.findFirstByOrderByDayNumberDescDateDesc();
        int deletedEntitiesCount = 0;
        if(!lastDayPredicted.isEmpty()){
            do{
                deletedEntitiesCount += 500;
                this.weatherPredictionDayRepository.deleteAll(this.weatherPredictionDayRepository.getByDayNumberLessThan(deletedEntitiesCount));  
            }while(deletedEntitiesCount < lastDayPredicted.get(0).getDayNumber());
        }
    }

    /**
     * @param dayNumber to find in the database.
     * @return a predicted day that meets the given number day
     */
    public WeatherPredictionDayDTO getByDay(int dayNumber){
        WeatherPredictionDay day = this.weatherPredictionDayRepository.findByDayNumberOrderByDateAndDayNumberAsc(dayNumber);
        if(day == null){
            return null;
        }
        return mapWeatherPredictionDayEntity.apply(day);
    }

    /**
     * @param weatherType filter of the weather
     * @return all the predicted days that meets the weather condition.
     */
    public List<WeatherPredictionDayDTO> getByWeather(String weatherType){
        List<WeatherPredictionDay> days = this.weatherPredictionDayRepository.findByWeatherTypeOrderByDateAscDayNumberAsc(WeatherType.fromString(weatherType).toString());
        return days.stream().map(mapWeatherPredictionDayEntity).collect(Collectors.toList());
    }

    /**
     * 
     * @return all the predicted days
     */
    public List<WeatherPredictionDay> getAll(){
        return this.weatherPredictionDayRepository.findAllOrderByDateAndDayNumberAsc();
    }

    /**
     * Predict the weather of every day from the initial date to the last day that must be ten years in the future.
     */
	public void predictForTheNextTenYears() {
        LocalDateTime dateTime = this.getNewDateInstance();
        long quatityOfDaysToPredict = ChronoUnit.DAYS.between(this.getNewDateInstance(), this.getCurrentDateInTenYears()) + 1;
        Long startDay = this.getStartDay(quatityOfDaysToPredict);
        if(startDay != null){
            dateTime = dateTime.plusDays(startDay);
            while(startDay <= quatityOfDaysToPredict){            
                if(startDay != 1){
                    dateTime = dateTime.plusDays(1);
                }
                this.weatherPredictionDayRepository.save(this.createWeatherPredictionDayEntity(startDay, dateTime));
                startDay++;
            }
        } 
	}

    private Function<WeatherPredictionDay, WeatherPredictionDayDTO> mapWeatherPredictionDayEntity = entity -> {
        return new WeatherPredictionDayDTO(entity.getDayNumber(), entity.getWeatherType());
    };

    /**
     * @return the inital date of the Existance
     */
    private LocalDateTime getNewDateInstance(){
        return LocalDateTime.of(2019, 1, 1, 0, 0, 0);
    }

    /**
     * Returns the start day to start predicting the weather. This function take in consideration that if
     * the given day was already predicted is not necessary to predict it. If the days to be predicted is less than the
     * last predicted day returns null, if is greater than the last predicted day it returns the last day predicted and
     * if there are no predicted days returns 0.
     * @param quatityOfDaysToPredict number of days to be predicted
     * @return the start day of the initial prediction day
     */
    private Long getStartDay(long quatityOfDaysToPredict) {
        List<WeatherPredictionDay> lastDayPredicted =  this.weatherPredictionDayRepository.findFirstByOrderByDayNumberDescDateDesc();
        long startDay = 0;
        if(!lastDayPredicted.isEmpty() && lastDayPredicted.get(0).getDayNumber() >= quatityOfDaysToPredict){
            return null;
        }
        if(!lastDayPredicted.isEmpty() && lastDayPredicted.get(0).getDayNumber() < quatityOfDaysToPredict){
            startDay = lastDayPredicted.get(0).getDayNumber() + 1 ;            
        }
        return startDay;
    }

    /**
     * 
     * @return the actual date more ten years in the future as LocalDateTime
     * 
     * @see LocalDateTime
     */
    protected LocalDateTime getCurrentDateInTenYears(){
        return LocalDateTime.now().plusYears(TEN_YEARS);
    }
}