package com.djorda.weatherprediction.dto;

import com.djorda.weatherprediction.model.WeatherType;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * WeatherPredictionDayDTO
 */
@Data
@AllArgsConstructor
public class WeatherPredictionDayDTO {

    @JsonProperty("dia")
    private int day;
    @JsonProperty("clima")
    private WeatherType weather;

    /**
     * @return the weather prediction of the day as a string
     */
    public String getWeather(){
        return this.weather.getWeatherCondition().toLowerCase();
    }
    
}