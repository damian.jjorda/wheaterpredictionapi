package com.djorda.weatherprediction.repository;

import java.util.List;

import com.djorda.weatherprediction.dto.WeatherPredictionDayDTO;
import com.djorda.weatherprediction.model.WeatherPredictionDay;

import org.springframework.cloud.gcp.data.datastore.repository.DatastoreRepository;
import org.springframework.cloud.gcp.data.datastore.repository.query.Query;
import org.springframework.data.repository.query.Param;
/**
 * Weather Prediction Day Repository to handle all the operations to the database
 * 
 * @see WeatherPredictionDay
 * @see WeatherPredictionDayDTO
 */
public interface WeatherPredictionDayRepository extends DatastoreRepository<WeatherPredictionDay, Long> {
    
    /**
     * @return all the predicted days order by Date and day number.
     */
    @Query("SELECT * FROM predictions ORDER BY date, dayNumber asc")
    List<WeatherPredictionDay> findAllOrderByDateAndDayNumberAsc();

    /**
     * @param dayNumber day number to find in the database
     * @return a predicted day order by date and day number by a day number given.
     */
    @Query("SELECT * FROM predictions WHERE dayNumber = @dayNumber ORDER BY date, dayNumber asc")
    WeatherPredictionDay findByDayNumberOrderByDateAndDayNumberAsc(@Param("dayNumber") int dayNumber);

    /**
     * @return a list of predicted days that matches the given weather type ordered by date and day number
     */
    List<WeatherPredictionDay> findByWeatherTypeOrderByDateAscDayNumberAsc(String weatherType);

    /**
     * @return first WeatherPredicionDay entity that has the maximum day number
     */
    List<WeatherPredictionDay> findFirstByOrderByDayNumberDescDateDesc();

    /**
     * used to retrieve a quantity of entities with day number less than the given parameter
     */
    @Query("SELECT * FROM predictions WHERE dayNumber < @dayNumber ORDER BY dayNumber asc, date asc")
    Iterable<WeatherPredictionDay> getByDayNumberLessThan(@Param("dayNumber") int dayNumber);
}