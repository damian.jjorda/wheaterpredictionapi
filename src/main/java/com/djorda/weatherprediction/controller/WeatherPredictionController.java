package com.djorda.weatherprediction.controller;

import java.util.List;
import java.util.concurrent.CompletableFuture;

import com.djorda.weatherprediction.dao.WeatherPredictionDayDAO;
import com.djorda.weatherprediction.dto.WeatherPredictionDayDTO;
import com.djorda.weatherprediction.model.WeatherPredictionDay;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/clima")
public class WeatherPredictionController {

    @Autowired
    private WeatherPredictionDayDAO weatherPredictionDayDAO;

    @GetMapping(value = "")
    @ResponseStatus(HttpStatus.OK)
    public WeatherPredictionDayDTO getByDay(@RequestParam(value = "dia") int day){
        return weatherPredictionDayDAO.getByDay(day);
    }

    @GetMapping(value = "/{weatherType}")
    @ResponseStatus(HttpStatus.OK)
    public List<WeatherPredictionDayDTO> getByWeatherType(@PathVariable(value = "weatherType") String type){
        return weatherPredictionDayDAO.getByWeather(type);
    }

    @GetMapping(value = "/todos")
    @ResponseStatus(HttpStatus.OK)
    public List<WeatherPredictionDay> getAll(){
        return weatherPredictionDayDAO.getAll();
    }

    @PostMapping(value = "/predecir")
    @ResponseStatus(HttpStatus.OK)
    public String predictForTheNextTenYears() {
        CompletableFuture.runAsync( () -> weatherPredictionDayDAO.predictForTheNextTenYears() );
        return "Prediciendo el clima!!";
    }

    @PostMapping(value = "/predecir/{numberDay}")
    @ResponseStatus(HttpStatus.OK)
    public WeatherPredictionDay predictDay(@PathVariable(value = "numberDay") Long numberDay) {
        return weatherPredictionDayDAO.save(numberDay);
    }

    @DeleteMapping(value = "/borrar/{id}")
    @ResponseStatus(HttpStatus.OK)
    public void delete(@PathVariable(value = "id") Long id){
        weatherPredictionDayDAO.deleteById(id);
    }

    @DeleteMapping(value = "/borrar")
    @ResponseStatus(HttpStatus.OK)
    public void deleteAll() {
        weatherPredictionDayDAO.deleteAll();
    }

}
