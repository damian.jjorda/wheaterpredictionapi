package com.djorda.weatherprediction.utils;

import java.math.BigDecimal;

/**
 * Helps to creates and manipulate BigDecimals
 */
public class BigDecimalUtil {

    /**
     * Returns a BigDecimal containing rounded decimals, of the given value that allows to compare 
     * against another BigDecimal with the same characteristics 
     * 
     * @param value the double number to be created as BigDecimal and rounded
     * 
     * @return BigDecimal that represents the given value
     * 
     * @see #BigDecimal
     * @since 1.0
     */
    public static BigDecimal getBigDecimalWithScale(double value){
        return new BigDecimal(value).setScale(2, BigDecimal.ROUND_HALF_UP);
    }
}