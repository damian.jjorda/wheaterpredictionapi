package com.djorda.weatherprediction.model;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.google.cloud.Timestamp;

import org.springframework.cloud.gcp.data.datastore.core.mapping.Entity;
import org.springframework.data.annotation.Id;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Represents a day with the prediction of the weather
 * 
 * @author Damian Jorda
 * @since 1.0
 */
@AllArgsConstructor
@NoArgsConstructor
@Entity(name = "predictions")
@Data
public class WeatherPredictionDay {
    
    @Id
    private Long id;
    private Timestamp date;
    private int dayNumber;
    private WeatherType weatherType;

    @JsonGetter(value = "date")
    public String getDate(){
        return this.date == null ? "" : this.date.toDate().toString();
    }

}