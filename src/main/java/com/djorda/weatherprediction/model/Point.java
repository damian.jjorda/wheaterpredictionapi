package com.djorda.weatherprediction.model;

import java.math.BigDecimal;
import java.text.DecimalFormat;

import com.djorda.weatherprediction.utils.BigDecimalUtil;

/**
 * Represents a location in a plane. It must contains coordinates {@code (X,Y)}
 * specified in BigDecimal precision.
 * 
 * @author Damian Jorda
 * @since 1.0
 */
public class Point {


	/** Represents the value of square in integer precision. */
    private final int SQUARE = 2;

    /**
     * The X coordinate of this <code>Point</code>.
     *
     * @see #getX()
     * @since 1.0
     */
    private BigDecimal x;

    /**
     * The Y coordinate of this <code>Point</code>.
     *
     * @see #getY()
     * @since 1.0
     */
    private BigDecimal y;

    /**
     * Constructs and initializes a point at the specified
     * coordinates {@code (x,y)}.
     * @param x the X coordinate of the new <code>Point</code>
     * @param y the Y coordinate of the new <code>Point</code>
     * @since 1.0
     */
    public Point(double x, double y){
        this.x = new BigDecimal(x).setScale(2, BigDecimal.ROUND_HALF_UP);
        this.y = new BigDecimal(y).setScale(2, BigDecimal.ROUND_HALF_UP);
    }

    /**
     * Constructs and initializes a point at the specified
     * coordinates {@code (x,y)} with a BigDecimal precision.
     * @param x the X coordinate of the new <code>Point</code>
     * @param y the Y coordinate of the new <code>Point</code>
     * @since 1.0
     */
    public Point(BigDecimal x, BigDecimal y){
        this.x = x;
        this.y = y;
    }

    /**
     * Returns the X coordinate of this <code>Point</code>
     * @return the X coordinate of this <code>Point</code>.
     * @since 1.0
     */
    public BigDecimal getX(){
        return this.x;
    }

    /**
     * Returns the Y coordinate of this <code>Point</code>
     * @return the Y coordinate of this <code>Point</code>.
     * @since 1.0
     */
    public BigDecimal getY(){
        return this.y;
    }

    /**
     * Returns the distance  between 2 <code>Point</code> measured in kilometers
     *  specified in double precision
     * 
     * @param point the <code>Point</code> to measure the distance
     * 
     * @return the distance between the current <code>Point</code> to the given <code>Point</code> 
     * 
     * @since 1.0
     */
    public BigDecimal distanceTo(Point point){
        double xPowDifference = Math.pow(point.getX().doubleValue() - this.getX().doubleValue(), SQUARE);
        double yPowDifference = Math.pow(point.getY().doubleValue() - this.getY().doubleValue(), SQUARE);
        return BigDecimalUtil.getBigDecimalWithScale(Math.sqrt(xPowDifference + yPowDifference));
    }

    /**
     * Returns if 2 Points are the same. They will be equals
     * if their coordinates are equals too.
     * 
     * @param point the <code>Point</code> to measure the distance
     * 
     * @return a boolean that represents if the given Point is the same 
     * 
     * @since 1.0
     */
    @Override
    public boolean equals(Object point) {
        if (point == null || !(point instanceof Point))
            return false;
        if (point == this)
            return true;
        Point otherPoint = (Point)point;
        return this.getX().compareTo(otherPoint.getX()) == 0 
            && this.getY().compareTo(otherPoint.getY()) == 0;
    }

    @Override
    public String toString() {
        return "(" + new DecimalFormat("#.##").format(this.getX()) + " , " + new DecimalFormat("#.##").format(this.getY()) + ")";
    }

}