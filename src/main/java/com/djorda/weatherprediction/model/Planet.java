package com.djorda.weatherprediction.model;

/**
 * Represents a <code>Planet</code> which orbits around the center of the <code>Solar System</code> 
 * that belongs. This movement is allowed thanks to the angular velocity and the distance to the center of the <code>Solar System</code> 
 * in addition to the time that the planet has moved.
 * 
 * @author Damian Jorda
 * @since 1.0
 * @see Point
 * @see SolarSystem
 */
public class Planet {


    /**
     * The angular velocity of this <code>Planet</code>.
     * That is needed to calculate the position of the planet in a given time.
     *
     * @since 1.0
     */
    private double angularVelocity;

    /**
     * The name of this <code>Planet</code>.
     *
     * @see #getName()
     * @since 1.0
     */
    private String name;

    /**
     * The distance to the sun that represents the Center 
     * of the Solar System to this <code>Planet</code>. That is 
     * needed to calculate the position of the planet in a given time.
     * 
     * @see SolarSystem
     * @see #getDistanceToSun()
     * @since 1.0
     */
    private double distanceToSun;


    /**
     * Constructs and initializes a planet with a given name,
     * distance to the sun and an angular velocity. Angular velocity and the distance 
     * to the sun are neccesary to calculate the position of the planet. The angular velocity
     * is represented by the amount of movement that the planet have by day, degrees per day.
     * <p>
     * The orbit of the planet is round.
     * @param name the name of the new <code>Planet</code>
     * @param angularVelocity the angular velocity of the new <code>Planet</code> by day
     * @param distanceToSun the distance to the sun of the new <code>Planet</code>
     * 
     * @see #getPostion
     * 
     * @since 1.0
     */
    public Planet(String name, double angularVelocity, double distanceToSun){
        this.angularVelocity = angularVelocity;
        this.name = name;
        this.distanceToSun = distanceToSun;
    }

    /**
     * @return the name of this <code>Planet</code>.
     * @since 1.0
     */
    public String getName(){
        return this.name;
    }

    /**
     * @return the distance to the sun of this <code>Planet</code>.
     * @since 1.0
     */
    public double getDistanceToSun(){
        return this.distanceToSun;
    }

    /**
     * Returns the position of the planet given an elapsed time in days.
     * 
     * @return the <code>Point</code> where the planet is
     * @since 1.0     
     * @see #Point
     */
    public Point getPosition(int day){
        double xCoordinate = distanceToSun * Math.cos(Math.toRadians(this.angularVelocity) * day);
        double yCoordinate = distanceToSun * Math.sin(Math.toRadians(this.angularVelocity) * day);
        return new Point(xCoordinate, yCoordinate);
    }

}