package com.djorda.weatherprediction.model;

import java.math.BigDecimal;

/**
 * Represents all the Points between two LinearEquations. One LinearEquation
 * will be the bottom limit of the semiplane. The other LinearEquation will be
 * the top limit of the semiplane.
 * 
 * @author Damian Jorda
 * @since 1.0
 * @see LinearEquation
 */
public class SemiPlane implements IContainer {

    private static final BigDecimal VERTICAL_SLOPE = null;

    /**
     * LinearEquation A that is used to create the Semiplane
     */
    private LinearEquation bottomLinearEquation;

    /**
     * LinearEquation B that is used to create the Semiplane
     */
    private LinearEquation topLinearEquation;

    /**
     * Constructs and initializes a SemiPlane.
     * The semiplane will choose which Linear Equation is the top limit and the bot limit, as well.
     * 
     * @param linearEquationA first limit of the semiplane
     * @param linearEquationB second limit of the semiplane
     * @since 1.0
     * @see LinearEquation
     */
    public SemiPlane(LinearEquation linearEquationA, LinearEquation linearEquationB){
        this.chooseLinearEquationsPosition(linearEquationA, linearEquationB);
    }

    /**
     * Choose which LinearEquations if the top limit and the bot limit. To choose the correct linearEquation
     * one point of the second linear equation is evaluated in the first linear equation to know if that point
     * is greater or lower than the first linear equation. If the point is greater then the first linearEquation
     * will be the bottom limit and the second linearEquation will be the top limit and the same otherwise
     * 
     * @param linearEquationA first linear Equation to check position
     * @param linearEquationB second linear Equation to check position
     */
    private void chooseLinearEquationsPosition(LinearEquation linearEquationA, LinearEquation linearEquationB) {        
        boolean islinearEquationBGreaterThanA = 0 <= this.evaluatePoint(linearEquationB.getPoint(), linearEquationA);        
        if(islinearEquationBGreaterThanA) {
            this.initializeLinearEquations(linearEquationA, linearEquationB);
        }else{
            this.initializeLinearEquations(linearEquationB, linearEquationA);
        }
    }

    /**
     * Initialize the bottomLinearEquation and topLinearEquation properties
     * 
     * @param bottomLinearEquation bottom limit linearEquation
     * @param topLinearEquation top limit linearEquation
     */
    private void initializeLinearEquations(LinearEquation bottomLinearEquation, LinearEquation topLinearEquation) {
        this.bottomLinearEquation = bottomLinearEquation;
        this.topLinearEquation = topLinearEquation;
    }

    /**
     * Returns if the given Point meets the conditions of the bottom and top Linear
     * Equations. If the point belongs to any of the LinearEquations of is in the
     * middle of them it will return true. To calculate if the point is in the
     * midle. Is checked <code>Y>=mX+b</code> for the bottom LinearEquation and
     * <code>Y<=mX+b</code> for the top LinearEquation.
     * 
     * @param point Point to check if belongs to the SemiPlane
     * @return true if the point belongs to any of the LinearEquations or is between
     *         them
     * @see Point
     */
    @Override
    public boolean belongs(Point point) {
        if(this.bottomLinearEquation.belongs(point) || this.topLinearEquation.belongs(point)){
            return true;
        }
        boolean isGreater = 0 <= this.evaluatePoint(point, this.bottomLinearEquation);
        boolean isSmaller = 0 >= this.evaluatePoint(point, this.topLinearEquation);
        return isGreater && isSmaller;
    }

    /**
     * Returns the result of the equation <code>mX+b-Y</code>. So if the result
     * value is less than 0 means that the point is <code>UP</code> the linear equation, if is 0
     * means that belongs to the linear equation and if is greater than 0 means that 
     * it is <code>UNDER</code> the linear equation. 
     * 
     * @param point Point to check against the LinearEquation
     * @param limit LinearEquation needed to check the point
     * @return the value of the point in the given Linear Equation
     * @see Point
     * @see LinearEquation
     */
    private double evaluatePoint(Point point, LinearEquation limit){
        if(limit.getSlope() == VERTICAL_SLOPE){
            return point.getX().subtract(limit.getPoint().getX()).doubleValue();
        }
        return point.getY().subtract(limit.getSlope().multiply(point.getX()).add(limit.getB())).doubleValue();
    }

}