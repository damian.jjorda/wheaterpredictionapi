package com.djorda.weatherprediction.model;

import java.util.List;

/**
 * A system solar configuration 
 * 
 * @see SolarSystem
 */
public interface ISolarSystemConfiguration {

    /**
     * Returns a list of Planets that belongs to the Solar System
     * 
     * @see Planet
     */
    List<Planet> getPlanets();

    /**
     * Returns how many days the Solar System has in one year.
     * 
     */
    int getDaysInYear();
}