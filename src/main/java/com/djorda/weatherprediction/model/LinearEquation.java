package com.djorda.weatherprediction.model;

import java.math.BigDecimal;

import com.djorda.weatherprediction.utils.BigDecimalUtil;


/**
 * Represents a straight line between 2 given <code>Points</code>. It is
 * possible that the Linear Equation also contains others points but is needed
 * that those points fulfill the equation that represents the
 * <code>LinearEquation</code>. This is an object representation of <code>Y=mX+b</code>
 * 
 * @author Damian Jorda
 * @since 1.0
 * @see Point
 */
public class LinearEquation implements IContainer{

    private static final BigDecimal VERTICAL_SLOPE = null;
    private static final BigDecimal HORIZONTAL_SLOPE = new BigDecimal(0).setScale(2, BigDecimal.ROUND_HALF_UP);

    /**
     * Point A that is used to create the Linear Equation
     */
    private Point pointA;

    /**
     * Point B that is used to create the Linear Equation
     */
    private Point pointB;

    /**
     * Slope of the current Linear Equation, 
     * slope = (y2 - y1)/(x2 - x1)
     */
    private BigDecimal slope;

    /**
     * Intersection point with the Y axis. means coordinates (0, b)
     */
    private BigDecimal b;

    /**
     * Constructs and initializes a LinearEquation
     * 
     * @since 1.0
     */
    public LinearEquation(Point pointA, Point pointB){
        this.pointA = pointA;
        this.pointB = pointB;
        this.calculateSlope();
        this.calculateIntersectionYPoint();
    }

    /**
     * Calculate the b Point of the Linear Equation which represents the
     * intersection between the Linear Equation and the Y axis
     */
    private void calculateIntersectionYPoint() {
        if(this.slope == VERTICAL_SLOPE){
            return;
        } 
        this.b = this.pointA.getY().subtract(this.slope.multiply(this.pointA.getX())).setScale(2, BigDecimal.ROUND_HALF_UP); 
    }

    /**
     * Calculate the slope of the Linear Equation by dividing the substraction of
     * the Y coordinates between pointB and pointA and the substraction of the X
     * coordinates between pointB and pointA. If the X coordinates are the same 
     * it means that it is a straight line on the Y axis
     */
    private void calculateSlope() {
        if(this.pointB.getX().compareTo(this.pointA.getX()) == 0){
            return;
        }
        BigDecimal dividend = this.pointB.getY().subtract(this.pointA.getY());
        BigDecimal divisor = this.pointB.getX().subtract(this.pointA.getX());
        this.slope = BigDecimalUtil.getBigDecimalWithScale(dividend.divide(divisor, 2 , BigDecimal.ROUND_HALF_UP).doubleValue());
    }

    /**
     * Returns if the given Point meets the conditions of the Linear Equation.
     * Using the general equation Y = mX + b where m is the slope and b is the 
     * intersection with the Y axis. <code>(X, Y)</code> represents the coordinates
     * of the given point.
     * @param point Point to check if belongs to the Linear Equation
     * @return true if the point meets the conditions
     * @see Point
     */
    @Override
    public boolean belongs(Point point) {
        if(this.slope == VERTICAL_SLOPE){
            return point.getX().compareTo(this.pointA.getX()) == 0;
        }
        if(this.slope.compareTo(HORIZONTAL_SLOPE) == 0) {
            return point.getY().compareTo(this.b) == 0;
        }
        return point.getY().compareTo(this.slope.multiply(point.getX()).add(this.b)) == 0;
    }

    /**
     * @return the b value of the equation <code>Y=mX+b</code>
     * @since 1.0
     */
    public BigDecimal getB(){
        return this.b;
    }

    /**
     * @return the slope of the equation which is the m of the linear equation <code>Y=mX+b</code>
     * @since 1.0
     */
    public BigDecimal getSlope(){
        return this.slope;
    }

    /**
     * Does the needed calculations to create a new Linear Equation that is parallel
     * to this and contains the given Point. The new Linear equation is created with
     * the given Point and the intersection with the Y axis.
     * <p>
     * If the given point belongs to the current Linear Equation, returns the same Linear Equation.
     * @param point Point to create a new Linear Equation that must be parallel to this one
     * @return  a new Linear Equation that is parallel to this one and contains the given Point
     * @see LinearEquation
     * @see Point
     */
    public LinearEquation getParallelLinearEquation(Point point) {
        if(this.belongs(point)){
            return this;
        }
        return new LinearEquation(point, this.getNewPointForParraelLinearEquation(point));
    }

    /**
     * Creates a new point needed for the creation of a Parallel Linear Equation.
     * If the slope is null, not exists, means that the current LinearEquation is a vertical line
     * so it will return a point that is vertical to the given point and parallel, as well.
     * If the slope exists we got a new b from <code>Y=mX+b</code> to <code>b=Y-mX</code> that
     * represents a Point that intersects with the Y axis.
     * <p>
     * If the given point belongs to the intersection with the Y axis, the slope exists and slope != 1 
     * it will return a point intersecting with the X axis. In the other hand if slope == 0 then it
     * will return a point that Y coordinate is the same as the given Point and other X coordinate.
     * 
     * @param point Point from which the new Point will be created
     * @return  a new Point that is needed to create a Parallel Linear Equation
     *
     * @see Point
     */
    private Point getNewPointForParraelLinearEquation(Point point) {
        if(this.slope == VERTICAL_SLOPE){
            return new Point(point.getX(), this.pointA.getY().compareTo(point.getY()) == 0 ? this.pointB.getY() : this.pointA.getY()); 
        }else{
            if(point.getX().compareTo(HORIZONTAL_SLOPE) == 0){            
                if(!(this.slope.compareTo(HORIZONTAL_SLOPE) == 0)){ 
                    double newB = point.getX().subtract(this.slope.multiply(point.getY())).doubleValue();
                    return new Point(newB, 0);
                }else{
                    return new Point(!(this.pointA.getX().compareTo(point.getX()) == 0) ? this.pointA.getX() : this.pointB.getX(), point.getY());
                }
            }else{
                double newB = point.getY().subtract(this.slope.multiply(point.getX())).doubleValue();
                return new Point(0, newB);
            }
        }
    }

    /**
     * Returns one of the points that belong to the LinearEquation. Which must meet the
     * conditions of it.
     * 
     * @see Point
     * @see LinearEquation
     * 
     * @return a point that belongs to the LinearEquation
     */
    public Point getPoint(){
        return this.pointA;
    }

    /**
     * Does the needed calculations to create a new Linear Equation that is perpendicular
     * to this and contains the given Point. The point must not belong to this LinearEquation.
     * 
     * @param point Point to create a new Linear Equation that must be perpendicular to this one
     * @return  a new Linear Equation that is perpendicular to this one and contains the given Point
     * @see LinearEquation
     * @see Point
     */
    public LinearEquation getPerpendicularLinearEquation(Point point) {
        if(this.belongs(point)){
            return null;
        }
        return new LinearEquation(point, this.getIntersectionPoint(this, point));
    }

    /**
     * Returns the intersection point between the linear equation and the given point. The new point will 
     * create a vertical Linear Equation with the given one. In order to generate the new point, first we have to calculate the new slope
     * which must be opposite and inverse of the given linearEquation, next the <code>B</code> value from <code>Y=mX+B => B=Y-mX</code>.
     * The next step is calculat the x coordinate using <code>Y1=m1*X1+b1 && Y2=m2*X2+b2 => Y2 = Y1 && X2 = X1 => m1*X+b1 = m2*X+b2 => 
     * X = (b2 - b1) / (m1 - m2) </code>. Once we have the X coordinate we use that value in the linear equation to find out Y coordinate
     * with <code>Y=mX+b</code>.
     * <p>
     * If the slope of the linear equation is null, it means that is a vertical line and the new point will have the x coordinate of the points of
     * the linear equation and the y coordinate of the given point.
     * <p>
     * If the slope of the linear equation is 0, it means that is a horizontal line and the new point will have the y coordinate of the points of
     * the linear equation and the x coordinate of the given point.
     * 
     * @param linearEquation which the intersection point belongs.
     * @param point from which the new point is generated.
     * @return the Point that intersects between the given linear equation and the point
     */
    // TODO throws exception when the point belongs to the linear equation
    public Point getIntersectionPoint(LinearEquation linearEquation, Point point) {  
        if(linearEquation.getSlope() == VERTICAL_SLOPE){
            return new Point(linearEquation.getPoint().getX(), point.getY());
        }
        if(linearEquation.getSlope().compareTo(HORIZONTAL_SLOPE) == 0){
            return new Point(point.getX(), linearEquation.getPoint().getY());
        }      
        double newSlope = -1 / linearEquation.getSlope().doubleValue(); 
        double newB = point.getY().doubleValue() - ( newSlope * point.getX().doubleValue() );
        double xCoordinate = ( newB - linearEquation.getB().doubleValue() ) / ( linearEquation.getSlope().doubleValue() - newSlope );
        double yCoordinate = ( newSlope * xCoordinate ) + newB;
        return new Point(xCoordinate, yCoordinate);        
    }
}