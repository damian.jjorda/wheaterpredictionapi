package com.djorda.weatherprediction.model;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Represents a solar system that can contains planets. Is the representacions of a cartesian plane
 * with a center represented by the sun with coordinates <code>(0,0)</code>.
 * 
 * @author Damian Jorda
 * @since 1.0
 */
public class SolarSystem {


    private static final int MIN_PLANETS_FOR_RAIN = 3;
    private static final int MIN_PLANETS_FOR_DROUGHT_OR_OPTIMUS = 2;
    private static final double CENTER_X_COORDINATE = 0;
    private static final double CENTER_Y_COORDINATE = 0;

    /**
     * A list of planets that belongs to this SolarSystem
     *
     * @since 1.0
     */
    private List<Planet> planets;

    /**
     * Center of the Solar System that is represented by the sun
     *
     * @since 1.0
     */
    private Point sun;

    /**
     * Constructs and initializes a Solar System by the given configuration.
     * 
     * @param config a needed configuration for the Solar System
     * 
     * @since 1.0
     */
    public SolarSystem(ISolarSystemConfiguration config){
        this.sun = new Point(CENTER_X_COORDINATE, CENTER_Y_COORDINATE);
        this.planets = config.getPlanets();
    }

    /**
     * Calculate the positions of the planets by the given day. Then evaluate all the positions
     * to return a valid weather type.
     * <p>
     * It is necessary that the amount of planets be at least 3 to validate all the possible weather types.
     * @param day to calculate the positions of the planets.
     * @return the weather type by the given day.
     * 
     * @since 1.0
     */
    public WeatherType getWeather(int day){
        WeatherType weatherCondition = WeatherType.NO_DATA;
        List<Point> positions = this.planets.stream().map( planet -> planet.getPosition(day)).collect(Collectors.toList());
        weatherCondition = this.checkDroughtAndOptimusCondition(positions);
        if(weatherCondition == WeatherType.NO_DATA){
            weatherCondition = this.checkRainAndSunnyCondition(positions);
        }         
        return weatherCondition;
    }

    /**
     * Check if the given positions of the planets meets the conditions for raining, max raining or sunny 
     * weather type. With the first three positions a triangle will be created. If the sun is inside the triangle
     * the weather will be rain, also if the sun is in the center of the triangle the weather will be max rain
     * and if the sun is outside of the triangle the weather will be sunny.
     * <p>
     * It is necessary that the amount of planets be at least 3.
     * @param positions list of positions of the planets.
     * @return the type of weather if meets the conditions.
     * 
     * @since 1.0
     */
    private WeatherType checkRainAndSunnyCondition(List<Point> positions) {  
        if(positions.size() < MIN_PLANETS_FOR_RAIN){
            return WeatherType.NO_DATA;
        }      
        Triangle triangle = new Triangle(positions.get(0), positions.get(1), positions.get(2));
        boolean isRainCondition = triangle.belongs(sun);
        boolean isMaxRainCondition = isRainCondition && triangle.isMaxPerimeter();
        return isMaxRainCondition 
            ? WeatherType.MAX_RAIN 
            : isRainCondition ? WeatherType.RAIN : WeatherType.SUNNY; 
    }

    /**
     * Check the weather condition of the given positions. If all of the positions
     * belongs to the same Linear Equation the Weather type is drought, if also the
     * sun belongs to the same Linear Equation the weather is optimus. If none of
     * them belongs to the Linear Equation returns <code>WeatherType.NO_DATA</code>.
     * <p>
     * It is necessary that the amount of planets be greater than 1. 
     * @param positions list of positions of the planets.
     * @return the type of weather if meets the conditions.
     * 
     * @since 1.0
     */
    private WeatherType checkDroughtAndOptimusCondition(List<Point> positions) {
        if(positions.size() < MIN_PLANETS_FOR_DROUGHT_OR_OPTIMUS){
            return WeatherType.NO_DATA;
        }
        LinearEquation linearEquation = new LinearEquation(positions.get(0), positions.get(1));
        boolean isOptimusCondition = positions.stream().allMatch(position -> linearEquation.belongs(position));
        boolean isDroughtCondition = isOptimusCondition && linearEquation.belongs(this.sun);  
        return isDroughtCondition 
            ? WeatherType.DROUGHT 
            : isOptimusCondition ? WeatherType.OPTIMUS : WeatherType.NO_DATA;       
    }

}