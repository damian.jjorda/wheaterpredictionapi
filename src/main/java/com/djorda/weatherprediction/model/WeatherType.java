package com.djorda.weatherprediction.model;

import java.util.HashMap;
import java.util.Map;

/**
 * Represents the weather condition
 * 
 * @author Damian Jorda
 * @since 1.0
 */
public enum WeatherType{
    NO_DATA("No hay informacion"),
    DROUGHT("sequia"),
    RAIN("lluvia"),
    MAX_RAIN("pico maximo de lluvia"),
    OPTIMUS("condicion optima de presion y temperatura"),
    SUNNY("soleado");

    /**
     * String representation of the weather condition.
     * 
     * @see #getWeatherCondition()
     */
    private String weatherCondition;

    /**
     * Constructs and initializes a WeatherType.
     * @param weatherCondition the text representation of the weather type.
     * @since 1.0
     */
    private WeatherType(String weatherCondition){
        this.weatherCondition = weatherCondition;
    }

    /**
     * Return the weather condition of the current <code>WeatherType</code>
     * as a String
     */
    public String getWeatherCondition() {
        return this.weatherCondition;
    }

    /**
     * @param weatherCondition to be mapped to a WeatherType enum
     * @return WeatherType enum for the given weathercondition or null if it doesn't belongs to WeatherType enum
     */
	public static WeatherType fromString(String weatherCondition) {	
		return weatherCondition == null || stringToWeatherTypeMap.get(weatherCondition) == null ? null : stringToWeatherTypeMap.get(weatherCondition);
	}

	private static final Map<String, WeatherType> stringToWeatherTypeMap = new HashMap<String, WeatherType>();
	static {
		for (WeatherType type : WeatherType.values()) {
			stringToWeatherTypeMap.put(type.weatherCondition, type);
		}
	}

}