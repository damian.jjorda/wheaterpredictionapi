package com.djorda.weatherprediction.model;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

/**
 * Configuration for the Coruscant Solar System
 * 
 * @author Damian Jorda
 * @since 1.0
 * @see Point
 */
@Component
public class CoruscantSolarSystemConfiguration implements ISolarSystemConfiguration {

    /**
     * All the planets that belongs to this Solar System.
     */
    private List<Planet> planets;
    /**
     * Quantity of days in one year.
     */
    private int daysInOneYear;

    /**
     * Constructs and initializes a CoruscantSolarSystemConfiguration.
     * 
     * @since 1.0
     */
    public CoruscantSolarSystemConfiguration() {
        this.planets = new ArrayList<>();
        this.createPlanets();
        this.daysInOneYear = 360;
    }

    /**
     * Create all the planets for the Coruscant Solar System.
     */
    private void createPlanets() {
        Planet ferengi = new Planet("Ferengi", -1, 500);
        Planet betasoide = new Planet("Betasoide", -3, 2000);
        Planet vulcano = new Planet("Vulcano", 5, 1000);
        this.planets.add(ferengi);
        this.planets.add(betasoide);
        this.planets.add(vulcano);
    }

    @Override
    public List<Planet> getPlanets() {
        return this.planets;
    }

    @Override
    public int getDaysInYear() {
        return this.daysInOneYear;
    }


}