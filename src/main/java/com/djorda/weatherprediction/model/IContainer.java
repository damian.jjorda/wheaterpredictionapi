package com.djorda.weatherprediction.model;

/**
 * A container that can contains <code>Points</code> within itself
 * 
 * @see Point
 */
public interface IContainer {

    /**
     * Returns if the given point belongs to the container
     * 
     * @see Point
     */
    boolean belongs(Point point);
}